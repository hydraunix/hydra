#include <drivers/disk.h>
#include <drivers/ahci.h>
#include <drivers/pci.h>
#include <arch/x86_64-elf/mm.h>
#include <fs/gpt.h>
#include <string.h>

static size_t disk_count;
static disk_node_t *disks_head;

int disk_read_bytes(disk_device_t *device, size_t offset, size_t count, uint8_t *dest)
{
	if (device->type == DISK_TYPE_AHCI) {
		hba_memory *abar = (hba_memory *) pci_bar5(device->pci_bus, device->pci_device);
		return ahci_read_bytes(&abar->ports[device->id], offset, count, dest);
	}

	return 0;
}

void disk_create_device(uint16_t pci_bus, uint16_t id, uint8_t pci_device, uint8_t type)
{
	if (!disks_head)
		disks_head = (disk_node_t *) k_page_zalloc(1);
	if (!(disk_count % 32))
		disks_head = (disk_node_t *) k_page_realloc(disks_head, (disk_count * sizeof(disk_node_t)) / 0x1000, ((disk_count + 32) * sizeof(disk_node_t)) / 0x1000);	

	disk_node_t *last = disks_head;
	for (size_t i=0; i < disk_count; i++) {
		if (!last->next)
			last->next = disks_head + (sizeof(disk_node_t) * disk_count);
		last = last->next;
	}

	disk_count++;
	
	disk_device_t disk = {
		.type = type,
		.pci_bus = pci_bus,
		.pci_device = pci_device,
		.id = id,
	};
	memcpy((uint8_t *) &last->device, (uint8_t *) &disk, sizeof(disk_device_t));

	if (type == DISK_TYPE_AHCI) {
		gpt_header header = {0};
		disk_read_bytes(&disk, 512, sizeof(gpt_header), &header);
		for (int i=0; i < header.entry_count; i++) {
			gpt_entry partition = {0};
			disk_read_bytes(&disk, (header.entries * 512) + (i * header.entry_size), header.entry_size, &partition);
			if (partition.type_guid[0] || partition.type_guid[1]) {
				printf("partition type: 0x%x%x\n", partition.type_guid[1], partition.type_guid[0]);
			}
		}
	}
}

uint64_t disk_get_id(disk_device_t *device)
{
	return ((uint64_t)device->id << 32) | ((uint64_t)device->pci_bus << 16)  | ((uint64_t)device->pci_device);
}

disk_device_t *disk_get_device(uint64_t id)
{
	disk_node_t *node = disks_head;
	for (size_t i=0; i < disk_count; i++) {
		if (disk_get_id(&node->device) == id)
			return &node->device;
		node = node->next;
	}

	return (disk_device_t *)0;
}

disk_node_t *disk_get_devices(size_t *count)
{
	*count = disk_count;
	return disks_head;
}
