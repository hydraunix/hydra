#ifndef GPT_H
#define GPT_H

#include <stddef.h>
#include <stdint.h>

typedef struct {
	uint8_t signature[8];
	uint32_t revision;
	uint32_t header_size;
	uint32_t checksum;
	uint32_t rsrv0;
	uint64_t header_lba;
	uint64_t alternate_lba;
	uint64_t first_block;
	uint64_t last_block;
	uint64_t guid[2];
	uint64_t entries;
	uint32_t entry_count;
	uint32_t entry_size;
	uint32_t entries_crc32;
} __attribute__((packed)) gpt_header;

typedef struct {
	uint64_t type_guid[2];
	uint64_t guid[2];
	uint64_t start;
	uint64_t end;
	uint64_t attributes;
	char name[];
} __attribute__((packed)) gpt_entry;

#endif
