#ifndef UFS_H
#define UFS_H

#include <stddef.h>
#include <stdint.h>

#define INODE_TYPE_FILE
#define INODE_TYPE_DIR
#define INODE_TYPE_LINK

typedef struct ufs_metadata {
	size_t inode_count;
} ufs_metadata_t;

typedef struct inode {
	uint8_t type; // file type
	size_t device; // device id
	size_t size; // file size in bytes
	size_t offset; // file start offset in disk
	size_t linked_inode; // inode to which this file is linked to (only if type is INODE_TYPE_LINK)
	size_t entry_count;
	struct inode *entries; // directory entries (only if file type is INODE_TYPE_DIR)
	size_t name_length;
	char name[];
} inode_t;

#endif
